**はじめに**
目的は、STUDY研究・調査ですが、
最終的に作るものは、建設資材リース向けのシステムです。

参加者全員に、developerの権限が与えられます。  
招待状が届いていない方は、連絡ください。　<<== どこに・・  
>slackのAgile459のメンバーに一括で招待状を届ける方法を教えてください。  
 >administratorになりたい方は立候補してください。

基本的にここで何をしてもokです。  
このファイルを書き直してもOKです。  

新しいファイルを作るのもよし、  
pull requestするのもOKです。
pull requestなしでpushしてもokです。

しばらくは、このグループで自由に使ってください。  

## Code of Conduct
下記のopenindianaのものに従います。  
[openindiana Code of Conduct](https://www.openindiana.org/community/code-of-conduct/)  
このページは、google clomeの翻訳機能でかなり正確に翻訳されます。

CoC違反について報告する場所は、今んとこないです。


### とりあえず私の頭のなかで決まっていること仕様
1. 仕様  
現行のシステムがあるのでとりあえずはそれに従います。  

1. 開発環境  
ubuntu+apache+php+cakePHP3+postgreSQL   
- ubuntu : WSLでも動く  
- apache php : ubuntuでインストールが簡単  
- cakePHP3 : もとのシステムがcakePHP2  
- postgresSQL : もとのシステムがpostgresSQL  
-               cakePHP3になるのでもとはあまり関係ないのだが  
-               単純に私がへそ曲がりなだけ  
cakePHPは、railsをまねして作られたフレームワークです。   
Laravel,rails他でやりたいという人が多い、またはメンターがいれば変わります。  

## Markdown
このREADME.mdは、Markdownと呼ばれる記法で記述されます。

